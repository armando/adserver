
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.jfkd.banner.util.FrequencyKeeper;
import org.jfkd.banner.util.FrequencyStorageCommand;
import org.prevayler.implementation.SnapshotPrevayler;


/**
 * 
 * 
**/
public class RebuildPrevayler{
    private static SnapshotPrevayler prevayler= null;
	private static FrequencyKeeper fk= null;
	public static void addLine(String log){
        try{
            StringTokenizer st= new StringTokenizer(log,"/");
            String premes= st.nextToken();
            String mes= st.nextToken();
            String get= st.nextToken();
            String uid= st.nextToken();
                FrequencyStorageCommand c= 
                    new FrequencyStorageCommand(uid);
                prevayler.executeCommand(c);
        }
		catch (Exception e){
		  System.out.println(e);
		}
	}



	public static void main(String[] args) {	
        String line=null;
		try {
            int max= Integer.parseInt(args[2]);
            prevayler= new SnapshotPrevayler(new FrequencyKeeper(max),args[1]);
            fk= (FrequencyKeeper) prevayler.system();
            BufferedReader br= new BufferedReader(new FileReader(new File(args[0])));                
            while ((line= br.readLine())!=null){
                addLine(line);
            }
            br.close();
            prevayler.takeSnapshot();
		}
		catch (Exception e){
			e.printStackTrace();
		}		
	}
}
