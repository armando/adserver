package org.jfkd.banner.util;

import org.prevayler.Command;
import org.prevayler.PrevalentSystem;

import java.io.Serializable;

public final class FrequencyStorageCommand implements Command {
  private final String freqId;

  public FrequencyStorageCommand(String freqId) {
    this.freqId = freqId;
  }

  public Serializable execute(PrevalentSystem system) throws Exception {
    ((FrequencyKeeper)system).keep(freqId);
    return null;
  }
}
