package org.jfkd.banner.util;

import java.util.*;
import java.io.*;
import java.text.*;
import java.net.URL;

import com.maxmind.geoip.Country;
import com.maxmind.geoip.LookupService;

public class CampaignStatus implements Comparable{
	private static final int UPDATE=200;
	private static final int MAXLOGLINE=400;
	private static final int LBUFFER=UPDATE*MAXLOGLINE;
	private static final String UNRESOLVED = "unresolved";

	private static String homedir;
	private static Locale uslocale=new Locale("en","US");

//Log files are US locale
	private final  SimpleDateFormat formatterLog=
		new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss",uslocale);
	private SimpleDateFormat formatterLogfile=
		new SimpleDateFormat("'log_'yyyy-MM-dd",uslocale);
	private DecimalFormat formatterRfc=new DecimalFormat("+00;-00");
		
// Optimization suggested by Dennis Sosnoski in 
// http://www-106.ibm.com/developerworks/library/jw-performance.html

	private final Date convertDate=new Date();
	private final StringBuffer convertBuffer=new StringBuffer();
	private final FieldPosition convertField=new FieldPosition(0);
	private final GregorianCalendar convertGc=new GregorianCalendar();
	private final StringBuffer convertBufferGc=new StringBuffer();
	private final FieldPosition convertFieldGc=new FieldPosition(0);
	
	private String campaignName, group;
    private CampaignFreq campaign;
	private String hour_filter, day_filter, user_filter;
    private String host_filters, host_filter, no_host_filter;
	private int id_count;
	private int start, end, impressions, imps, clicks;

	private double correction_base,tolerance_base,marketing;

	private int loglines=0; 
	private StringBuffer log=new StringBuffer(LBUFFER);
	private StringBuffer dateLog=null;
	private StringBuffer rfcLog=null;
	private long offset;
	
//  Para cuando haya frecuencias por grupo (no descabellado)    
//	private JDBMRecordManager recman=null;
//	private JDBMHashtable hashtable=null;

	public CampaignStatus (CampaignFreq campaign, String group){
		this.campaignName=campaign.getName();
		this.group=group;
        this.campaign=campaign;
		load();
	}

	public CampaignStatus (String campaign, String group, int start, int end, int impressions, int imps,int clicks){
		this.campaignName=campaign;
		this.group=group;
		this.start=start;
		this.end=end;
		this.impressions=impressions;
		this.imps=imps;
		this.clicks=clicks;
	}

	public String getCampaignName(){
		return campaignName;
	}

    public String getCampaign(){
        return campaignName;
    }
	
	public String getGroup(){
		return group;
	}
	
	public int getStart(){
		return start;
	}
	
	public int getEnd(){
		return end;
	}
	public int getImpressions(){
		return impressions;
	}
	public int getWeight(){
		return (impressions<=100)?impressions:0;
	}
	public int getImps(){
		return imps;
	}
	public int getClicks(){
		return clicks;
	}

	public synchronized void addImp(String remoteAddr,String userAgent,String page,String creativity,String id){
		imps++;
		if (id_count>0){
//            id=(id.equals("anon"))?remoteAddr+userAgent:id;
            campaign.addFreq(id);
		}
		addLog('L',remoteAddr,page,creativity,id);
	}

    public synchronized void addLog
    	(char impclk, String remote_addr, String page, String creativity, String id){

		long time=System.currentTimeMillis();
		convertDate.setTime(time);
		convertBuffer.setLength(0);
		dateLog=formatterLog.format(convertDate,convertBuffer,convertField);
		convertGc.setTime(convertDate);
		convertBufferGc.setLength(0);
		offset=(convertGc.get(Calendar.ZONE_OFFSET)+convertGc.get(Calendar.DST_OFFSET))/(1000*60*60);
		StringBuffer rfcLog=formatterRfc.format(offset,convertBufferGc,convertFieldGc).append("00");

		log.append(remote_addr);log.append(" - - [");
		log.append(dateLog.toString());log.append(" ");
//System.out.println(zoneRfc); 
		log.append(rfcLog.toString());log.append("] \"GET /");
		log.append(id);log.append("/");
		log.append(impclk);log.append("/");
		log.append(campaignName);log.append("/");
		log.append(group);log.append("/");
		log.append(page);log.append("/");
		log.append(creativity);log.append(" HTTP/1.0\" 200 -\n");
//System.out.println(log.toString());
		if (++loglines%UPDATE==0){
			save();
		}
	}	

	public synchronized void addClick(String remoteAddr,String page,String banner,String id){
		clicks++;
		addLog('C',remoteAddr,page,banner,id);
	}

	public boolean isValid(String remote_addr, String user_agent, String id, HashMap domains, LookupService ls){
		Calendar now = Calendar.getInstance();
		long nowsecs = now.getTime().getTime()/1000;
		int hour=now.get(Calendar.HOUR_OF_DAY);
		int day=now.get(Calendar.DAY_OF_WEEK);
		String freqId,domain;

		char hourc=Character.forDigit(hour,24);
		char dayc=Character.forDigit(day,8);
        
		if (nowsecs<start || nowsecs>end ){
//System.out.println(campaignName+ " en "+group+" fuera de fecha: "+nowsecs+" > "+end);
			return false;
		}

//System.out.println("Host_filter: "+host_filter+"-ID:"+id+".."+id_count);
//System.out.println("Hour: "+hour+","+hourc+"- day:"+day+","+dayc);
		if (!hour_filter.equals("*") && hour_filter.indexOf(hourc)==-1){
//System.out.println(campaignName+" fuera de hora "+hourc);
			return false;
		}

		if (!day_filter.equals("*") && day_filter.indexOf(dayc)==-1){
//System.out.println(campaignName+" fuera de dia "+dayc);
			return false;
		}
	
		if (!host_filters.equals("*")){
            domain=getDomain(remote_addr, domains, ls);
			if (!host_filter.equals("*") && host_filter.indexOf(domain)<0 || no_host_filter.indexOf(domain)>=0){
			return false;
			}
		}

		if (!user_filter.equals("*") && user_filter.indexOf(user_agent)==-1){
//System.out.println("No cuela: "+user_agent);
			return false;
		}
		
		if (id_count>0){
//            System.out.println("Id="+id);
//			id=(id.equals("anon"))?remote_addr+user_agent:id;
            if (!campaign.isValid(id)){
                return false;
            }
		}
		
		return true;
	}

	private String getDomain(String remoteAddr, HashMap domains, LookupService ls){
        if (ls != null){
            Country country= ls.getCountry(remoteAddr);
            return (country != null) ? country.getCode().toLowerCase(): UNRESOLVED;
        }
		StringTokenizer st=new StringTokenizer(remoteAddr,".");
		StringBuffer sb=new StringBuffer();
		String subdomain;
		
		while (st.hasMoreTokens()){
			sb.append(st.nextToken());
			subdomain=sb.toString();
//System.out.println("Testing : "+subdomain);
			if (domains.containsKey(subdomain)){
				return (String) domains.get(subdomain);
			}
			sb.append(".");
		}
		return UNRESOLVED;
	}


	public double getDiff(){
		Date now = new Date();
		long nowsecs = (long) (now.getTime()/1000);

		if (impressions<=100){
			return 0.0;
		}
		// Campaigns should have priority when getting to their end
//		double correction=correction_base*((double) end-start)/(end-nowsecs+1);
		double correction=correction_base*((double) 10000000.0)/(end-nowsecs+1);


		// Marketing: banners should start over average
		// Tolerance: we should be too strict on numbers
		double tolerance=tolerance_base + marketing/correction;

		double aim_average= (100.0*tolerance*impressions)/(end-start+1);
		double act_average= (100.0*imps)/(nowsecs-start+1);

/*
System.out.println("correction: "+correction);

System.out.print(campaignName+": aim="+aim_average+" act="+act_average);
System.out.println(":"+((aim_average - act_average)*correction));
*/
		return (aim_average - act_average)*correction;
	}

	public boolean isWeight(){
		return impressions<=100;
	}

	public String toString(){
		return "Campaign: "+campaignName+" Group: "+group+
			" Start: "+start+" End: "+end+
			" Impres: "+impressions+" Imps: "+
			imps+" Clk: "+clicks+ " Diff: "+getDiff();
	} 

	public int compareTo(Object o) {
		CampaignStatus gs=(CampaignStatus) o;
		double diff=getDiff();
		return (diff>gs.getDiff())?-1:1;
	}

	public synchronized void load(){
		String line;

		loadProperties();
		try {
			FileReader fr = new FileReader(homedir+"/control/"+campaignName+"/"+group+".control");
//			System.out.println("Camp:" + campaignName);
			BufferedReader br = new BufferedReader(fr);
			line=br.readLine();
			StringTokenizer st= new StringTokenizer(line," ");
			this.start=Integer.parseInt(st.nextToken());
//			System.out.println("Empieza a las "+this.start);
			this.end=Integer.parseInt(st.nextToken());
//			System.out.println("Termina a las "+this.end);
			this.impressions=Integer.parseInt(st.nextToken());
			br.close();

			fr = new FileReader(homedir+"/control/"+campaignName+"/"+group+".imp");
			br = new BufferedReader(fr);
			line=br.readLine();
			this.imps=(line!=null)?Integer.parseInt(line):0;
			br.close();

			fr = new FileReader(homedir+"/control/"+campaignName+"/"+group+".clk");
			br = new BufferedReader(fr);
			line=br.readLine();
			this.clicks=(line!=null)?Integer.parseInt(line):0;
			br.close();

			fr = new FileReader(homedir+"/control/"+campaignName+"/control");
			br = new BufferedReader(fr);
			line=br.readLine();
			st= new StringTokenizer(line," ");
            
			this.hour_filter=st.nextToken();
			this.day_filter=st.nextToken();
            
            this.host_filters=st.nextToken();
            int no_host=host_filters.indexOf("!");
            if (no_host>=0 && no_host<host_filters.length()){
    			this.host_filter=(no_host>0)?host_filters.substring(0,no_host):"*";
                this.no_host_filter=host_filters.substring(no_host+1);
            }
            else{
                this.host_filter=host_filters;
                this.no_host_filter="";
            }

			this.user_filter=st.nextToken();
//			System.out.println(campaignName+" host_filter="+host_filters+"("+host_filter+":"+no_host_filter+")");
			this.id_count=Integer.parseInt(st.nextToken());
			br.close();
		}
		catch (IOException e) {
			this.start=this.end=0;
			this.impressions=0;
			this.imps=0;
			this.clicks=0;
			this.hour_filter="";
			System.err.println("Error cargando campana: "+campaignName+":"+e);
		}
	}

	private void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			tolerance_base=Double.parseDouble(server.getProperty("tolerance"));
			marketing=Double.parseDouble(server.getProperty("marketing"));
			correction_base=Double.parseDouble(server.getProperty("correction"));

			homedir=server.getProperty("homedir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	public synchronized void save(){
        int length= 0;
		try {
			FileWriter fw = new FileWriter (homedir+"/control/"+campaignName+"/"+group+".imp");
			BufferedWriter bw = new BufferedWriter (fw);
			PrintWriter out= new PrintWriter(bw);
			out.println(this.imps);
			out.close();

			fw = new FileWriter(homedir+"/control/"+campaignName+"/"+group+".clk");
			bw = new BufferedWriter(fw);
			out= new PrintWriter(bw);
			out.println(this.clicks);
			out.close();

			fw = new FileWriter(homedir+"/control/"+campaignName+"/"+group+"."+
				formatterLogfile.format(new Date()),true);
			bw = new BufferedWriter(fw);
			out= new PrintWriter(bw);
			out.print(log.toString());
            length= log.length();
			log.delete(0,length);
			loglines=0;
			out.close();
		}
		catch (Exception e) {
			System.err.println("Error en save:"+e);
		}
		campaign.save();
		System.out.println("Grabando estado de "+campaignName+" - "+formatterLog.format(new Date())+ " con "+length);
	}

	public static void main(String args[]) {
		CampaignStatus n[] = {
			new CampaignStatus("a","b",1000,2000,100,5,0),
			new CampaignStatus("c","d",1000,2000,100,10,0),
			new CampaignStatus("e","f",1000,2000,100,15,0),
			new CampaignStatus("g","h",1000,2000,100,20,0)
		};
		List l = Arrays.asList(n);
		Collections.sort(l);
		System.out.println(l);
	}
}
