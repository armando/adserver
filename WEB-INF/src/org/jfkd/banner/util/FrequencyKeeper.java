package org.jfkd.banner.util;

import java.util.HashMap;
import java.util.Map;

import org.prevayler.implementation.AbstractPrevalentSystem;

public final class FrequencyKeeper extends AbstractPrevalentSystem {
    private final Map freqs = new HashMap();
    private int max=0;
  
    public FrequencyKeeper (int max){
        System.out.println("Abrimos registrador de frecuencias "+max);
        this.max=max;
    }

    public void keep(String freqId) {
        Integer c=(Integer) freqs.get(freqId);
        int count=0;
        if (c!=null){
            count=c.intValue();
//            System.out.println(count+":"+max+":"+freqId);
            if (count>=max){
                return;
            }
        }
        else{
//            System.out.println("Counter null en "+freqId);
        }
        synchronized (freqs){
            freqs.put(freqId,new Integer(count+1));
        }
    }

    public int getFreq(String freqId){
        if (freqs.containsKey(freqId)){
            return ((Integer) freqs.get(freqId)).intValue();
        }
        return 0; 
    }
}
