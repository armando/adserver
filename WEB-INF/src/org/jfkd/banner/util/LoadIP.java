package org.jfkd.banner.util;

import java.io.*;
import java.util.*;
import java.net.URL;

public class LoadIP
{
	protected static String resolutionFile;
	
	public static HashMap map(){
		String line;
		String ip,country;
		HashMap map=new HashMap();
		loadProperties();
		int i=0;

		try {
			File resol =new File(resolutionFile);
			if (resol.isFile()){
				FileReader fr = new FileReader(resol);
				BufferedReader br = new BufferedReader(fr);
System.out.print("Leyendo "+resolutionFile+"...");
				StringTokenizer st;

				while ((line=br.readLine()) != null){
					if (line.length()>1){
						st= new StringTokenizer(line," ");
						try{
							ip=st.nextToken();
							country=st.nextToken();
							map.put(ip,country);
							i++;
						}
						catch (NoSuchElementException e){
							System.out.println(e+" "+line);
						}
//					    System.out.println("--"+ip+":"+country+"--");
						st=null;
					}
				}
				br.close();
				fr.close();
				resol=null;
System.out.println(" con "+i+" clases");
			}
		}
		catch (IOException e) {
			System.err.println(e);
		}
		return map;
	}

	private static void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			resolutionFile=server.getProperty("resolution");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public static void main(String args[]){
		System.out.println("Trying..");
		HashMap m;
		m=map();		
	}
}
