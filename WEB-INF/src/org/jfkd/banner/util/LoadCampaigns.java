package org.jfkd.banner.util;

import java.io.*;
import java.util.*;
import java.net.URL;

public class LoadCampaigns
{
	protected static String homedir;
	
	public static List campaigns(String group){
		String line= new String();
		List list = new ArrayList();
		loadProperties();
//        System.out.println("Cargando "+group);

		try {
			File campaignsFile=new File(homedir+"/campanas/"+group);
			if (campaignsFile.isFile()){
				FileReader fr = new FileReader(campaignsFile);
				BufferedReader br = new BufferedReader(fr);
				while ((line=br.readLine()) != null){
					if (line.length()>1){
						list.add(line);
					}
				}
				br.close();
                fr.close();
			}
		}
		catch (IOException e) {
			System.err.println(e);
		}
		return list;
	}

	private static void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
            InputStream in=serverUrl.openStream();
			server.load(in);
		
			homedir=server.getProperty("homedir");
            in.close();
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}
}
