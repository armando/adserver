package org.jfkd.banner.util;

import java.util.*;
import java.io.*;
import java.net.URL;

public class Creativities{
	private String campaign;
	private ArrayList creativities=new ArrayList();
	private ArrayList weights=new ArrayList();
	private int num_creativities,total_weight=0;
	private String homedir,redirurl,bannerurl,bannerdir;

	public Creativities (String campaign){
		this.campaign=campaign;
		load();
	}

	public String getCampaign(){
		return campaign;
	}

	public String toString(){
		return "";
	} 

	public Creativity chooseCreativity(){
		Random rand=new Random();
		//Integer.MAX_VALUE is to mask off sign bit
		int n=1+(rand.nextInt() & Integer.MAX_VALUE)%total_weight;
		int weight=0;

		for (int i=0;i<num_creativities;i++){
			weight+= ((Integer) weights.get(i)).intValue();
			if (n<=weight){
				return (Creativity) creativities.get(i);
			}
		}
		return null;
	}

	public void load(){
		String line,file;
		loadProperties();

		try {
			FileReader fr = new FileReader(homedir+"/control/"+campaign+"/creatividades");
			BufferedReader br = new BufferedReader(fr);
//System.out.println("\nCreatividades de "+campaign+":");
			while ((line=br.readLine())!=null){
				if (line.length()>1){
					StringTokenizer st= new StringTokenizer(line,"|");
					String image=st.nextToken();
					file=(image.indexOf(".html")>0)?bannerFile(image):"";
					int weight=Integer.parseInt(st.nextToken());
					String url=st.nextToken();
					String alt=st.nextToken();
//System.out.println(campaign+":"+image+"-"+weight+"-"+url+"-"+alt);
					Creativity cr=new Creativity
						(campaign,bannerdir,bannerurl,image,redirurl,url,alt,file);
					creativities.add(cr);
					weights.add(new Integer(weight));
					total_weight+=weight;
				}
			}
			br.close();
		}
		catch (IOException e) {
			System.err.println("Creativities (load) "+campaign+":"+e);
			Creativity cr=new Creativity(campaign,"","","","","","Error","");
			creativities.add(cr);
			weights.add(new Integer(1));
			total_weight=1;
		}
		num_creativities=creativities.size();
	}

	private String bannerFile(String file){
		try{
			FileReader fr = new FileReader(bannerdir+"/"+campaign+"/"+file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			StringBuffer sb=new StringBuffer();
		
			while ((line=br.readLine())!=null){
			// We will eventually parse this file, of course
				sb.append(line);sb.append("\n");
			}
		
			return sb.toString();
		}
		catch (IOException e){
			System.err.println(e);
			return "";
		}
	}

	private void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			homedir=server.getProperty("homedir");
			redirurl=server.getProperty("redirurl");
			bannerurl=server.getProperty("bannerurl");
			bannerdir=server.getProperty("bannerdir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void main(String args[]) {
	}
}
