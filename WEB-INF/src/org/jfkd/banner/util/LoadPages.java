package org.jfkd.banner.util;

import java.io.*;
import java.util.*;
import java.net.URL;

public class LoadPages
{
	private static String homedir;

	public static ArrayList pages()
		throws FileNotFoundException
	{
		ArrayList list = new ArrayList();
		loadProperties();
		
		File directory=new File(homedir+"/grupos");
		String[] files=directory.list();
		if (files!=null){
			int nfiles=files.length;
			
			for (int i=0;i<nfiles;i++){
				if (files[i].indexOf(".")!=0){
					list.add(files[i]);
				}
			}
		}
		else{
			throw new FileNotFoundException("Error al abrir "+homedir+"/grupos");
		}
		return list;
	}	

	private static void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			homedir=server.getProperty("homedir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void main(String args[]) {
		try{
			System.out.println(pages());
		}
		catch (IOException e){
			System.err.println(e);
		}	
	}
}
