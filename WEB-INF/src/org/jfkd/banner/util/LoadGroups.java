package org.jfkd.banner.util;

import java.io.*;
import java.util.*;
import java.net.URL;

public class LoadGroups
{
	private static String homedir;
	
	public static ArrayList groups(String page){
//        System.out.println("Grupos de "+page);
		String line= new String();
		ArrayList list = new ArrayList();
		loadProperties();

		try{
			FileReader fr = new FileReader(homedir+"/grupos/"+page);
			BufferedReader br = new BufferedReader(fr);
			int cnt = 0;
			while ((line=br.readLine()) != null){
				list.add(line);
//System.out.println(line);
			}
			br.close();
/*
                StringTokenizer st= new StringTokenizer(page, "-");
                StringBuffer sb= new StringBuffer();
                while (st.hasMoreTokens()){
                    sb.append(st.nextToken());
                    list.add(sb.toString());
                    System.out.println("Nuevo grupo "+sb.toString());
                    sb.append("-");
                }
*/
		}
		catch (IOException e) {
			System.err.println(e);
		}
        if (page.indexOf('=')>0){
            list.add("estadisticas");
            System.err.println(page+"a estadisticas");
        }
		return list;
	}

	private static void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			homedir=server.getProperty("homedir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void main(String args[]) {
		String line= new String();

		if (args.length != 1) {
			System.err.println("missing filename");
			System.exit(1);
		}
		try {
			ArrayList list = new ArrayList();
			FileReader fr = new FileReader(args[0]);
			BufferedReader br = new BufferedReader(fr);
			int cnt = 0;
			while ((line=br.readLine()) != null){
				if (line.length()>1){
					list.add(line);
				}
				cnt++;
			}
			br.close();
			System.out.println(cnt);
			System.out.println(list);
		}
		catch (IOException e) {
			System.err.println(e);
		}
	}

}
