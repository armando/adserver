package org.jfkd.banner.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.StringTokenizer;

import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.CheckpointConfig;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;

public class CampaignJE implements CampaignFreq{
	private static String homedir;
	
	private String name;
	private String hour_filter, day_filter, user_filter;
    private String host_filters, host_filter, no_host_filter;
	private int id_count;

	private double correction_base,tolerance_base,marketing;
	
    private Environment freqEnv= null;
	private Database freqDb= null;

    public CampaignJE (String name){
		this.name=name;
		load();
		if (id_count>0){
			try{
		        EnvironmentConfig envConfig = new EnvironmentConfig();
		        envConfig.setTransactional(true); 
		        envConfig.setAllowCreate(true);    
                File envDir= new File(homedir+"/control/"+name);

		        freqEnv = new Environment(envDir, envConfig);
		        
		        /* Make a database within that environment */
		        Transaction txn = freqEnv.beginTransaction(null, null);
		        DatabaseConfig dbConfig = new DatabaseConfig();
		        dbConfig.setTransactional(true); 
		        dbConfig.setAllowCreate(true);
		        freqDb = freqEnv.openDatabase(txn, "cookies.je", dbConfig);
		        
		        txn.commit();
		        System.err.println("Abriendo frecuencias en "+envDir.toString());
			    
            }
            catch (Exception e){
				System.err.println("Error opening persistence in "+name);
			}
		}
	}

	public String getName(){
		return name;
	}
	
	public synchronized void addFreq(String freqId){
//	    System.out.println("--"+freqId+"--");
		if (freqId.equals("anon")){
			return;
		}
        
        int count=0, result=0;
        try{
            DatabaseEntry keyEntry = new DatabaseEntry();
            DatabaseEntry dataEntry = new DatabaseEntry();

            StringBinding.stringToEntry(freqId, keyEntry);
            OperationStatus status= freqDb.get(null, keyEntry, dataEntry, LockMode.DEFAULT);
            
            if (status == OperationStatus.SUCCESS) {
                count= IntegerBinding.entryToInt(dataEntry);
            }

            IntegerBinding.intToEntry(++count, dataEntry);
//            System.out.println("Actualizando contador a "+count);

//            Transaction txn = freqEnv.beginTransaction(null, null);
            
            status= freqDb.put(null, keyEntry, dataEntry);

//            txn.commit();

        }
        catch (Exception e){
            System.out.println("Excepcion en addFreq " + name + e);

        }        
	}

    public boolean isValid(String freqId){
        if (freqId.equals("anon")){
            return true;
        }
        int count=0;
        try{
            DatabaseEntry keyEntry = new DatabaseEntry();
            DatabaseEntry dataEntry = new DatabaseEntry();

            StringBinding.stringToEntry(freqId, keyEntry);
            OperationStatus s= freqDb.get(null, keyEntry, dataEntry, LockMode.DEFAULT);
            count= (s == OperationStatus.SUCCESS) ? IntegerBinding.entryToInt(dataEntry) : 0;

// System.out.println(name+":"+freqId+"="+count);

            if (count>=id_count){
                return false;
            }
        }
        catch (Exception e){
            System.err.println("Error en isValid JE " + name + e);
            return false;
        }
        
        return true;
    }    
        
	public synchronized void load(){
		String line;

		loadProperties();
		try {
			FileReader fr = new FileReader(homedir+"/control/"+name+"/control");
			BufferedReader br = new BufferedReader(fr);
			line=br.readLine();
			StringTokenizer st= new StringTokenizer(line," ");
            
			this.hour_filter=st.nextToken();
			this.day_filter=st.nextToken();
            
            this.host_filters=st.nextToken();
            int no_host=host_filters.indexOf("!");
            if (no_host>=0 && no_host<host_filters.length()){
    			this.host_filter=(no_host>0)?host_filters.substring(0,no_host):"*";
                this.no_host_filter=host_filters.substring(no_host+1);
            }
            else{
                this.host_filter=host_filters;
                this.no_host_filter="";
            }

			this.user_filter=st.nextToken();
//			System.out.println(name+" host_filter="+host_filters+"("+host_filter+":"+no_host_filter+")");
			this.id_count=Integer.parseInt(st.nextToken());
			br.close();
		}
		catch (IOException e) {
			this.hour_filter="";
			System.err.println("Error cargando campana: "+name+":"+e);
		}
	}

	private void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			tolerance_base=Double.parseDouble(server.getProperty("tolerance"));
			marketing=Double.parseDouble(server.getProperty("marketing"));
			correction_base=Double.parseDouble(server.getProperty("correction"));

			homedir=server.getProperty("homedir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}

	public void save(){
	    try {
            freqEnv.checkpoint(CheckpointConfig.DEFAULT);
        } catch (DatabaseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	protected void finalize(){
        if (id_count>0 && freqDb!=null){
            try {
                System.err.println("Cerrando frecuencias");
                freqDb.close();
                freqEnv.close();
            }
            catch (DatabaseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }                    
	}

	public static void main(String args[]) {
	}   
 }
