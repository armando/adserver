package org.jfkd.banner.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.StringTokenizer;

import org.prevayler.implementation.SnapshotPrevayler;

public class Campaign implements CampaignFreq{
    private static final int UPDATE=1000;
	private String homedir;
	
	private String name;
	private String hour_filter, day_filter, user_filter;
    private String host_filters, host_filter, no_host_filter;
	private int id_count,update_count=0;

	private double correction_base,tolerance_base,marketing;
    private SnapshotPrevayler prevayler;
    private FrequencyKeeper freq;

    public Campaign (String name){
		this.name=name;
		load();
		if (id_count>0){
			try{
                String fileName=homedir+"/control/"+name+"/cookies";
                FrequencyKeeper fk= new FrequencyKeeper(id_count);
                prevayler=new SnapshotPrevayler(fk, fileName);
                freq=(FrequencyKeeper) prevayler.system();
            }
            catch (Exception e){
				System.err.println("Error opening persistence in "+name+ e);
			}
		}
	}

	public String getName(){
		return name;
	}
	
	public void addFreq(String freqId){
//        System.out.println("freqid="+freqId);
		if (freqId.equals("anon")){
			return;
		}
        
        try{
            FrequencyStorageCommand c=new FrequencyStorageCommand(freqId);
            prevayler.executeCommand(c);
            c=null;
            if (update_count++ % UPDATE == 0){
                save();
            }
        }
        catch (Exception e){
            System.out.println("addFreq " + name + e);

        }        
	}

    public boolean isValid(String freqId){
//        System.out.println("freqid="+freqId);
        if (freqId.equals("anon")){
            return false;
        }

        try{
            int count=freq.getFreq(freqId);
            System.out.println(name+":"+count);
            if (count>=id_count){
                return false;
            }
        }
        catch (Exception e){
            System.err.println("[campaign] isValid " + name + e);
            return false;
        }
        
        return true;
    }    
        
	public synchronized void load(){
		String line;

		loadProperties();
		try {
			FileReader fr = new FileReader(homedir+"/control/"+name+"/control");
			BufferedReader br = new BufferedReader(fr);
			line=br.readLine();
			StringTokenizer st= new StringTokenizer(line," ");
            
			this.hour_filter=st.nextToken();
			this.day_filter=st.nextToken();
            
            this.host_filters=st.nextToken();
            int no_host=host_filters.indexOf("!");
            if (no_host>=0 && no_host<host_filters.length()){
    			this.host_filter=(no_host>0)?host_filters.substring(0,no_host):"*";
                this.no_host_filter=host_filters.substring(no_host+1);
            }
            else{
                this.host_filter=host_filters;
                this.no_host_filter="";
            }

			this.user_filter=st.nextToken();
			System.out.println(name+" host_filter="+host_filters+"("+host_filter+":"+no_host_filter+")");
			this.id_count=Integer.parseInt(st.nextToken());
			br.close();
		}
		catch (IOException e) {
			this.hour_filter="";
			System.err.println("Error cargando campana: "+name+":"+e);
		}
	}

	private void loadProperties(){
		Properties server = new Properties();
		try{
			URL serverUrl=Thread.currentThread().getContextClassLoader().getResource("server.properties");
			server.load(serverUrl.openStream());
		
			tolerance_base=Double.parseDouble(server.getProperty("tolerance"));
			marketing=Double.parseDouble(server.getProperty("marketing"));
			correction_base=Double.parseDouble(server.getProperty("correction"));

			homedir=server.getProperty("homedir");
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}


    public void save(){
        if (id_count>0 && prevayler!=null){
            try {
                prevayler.takeSnapshot();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }                    
    }

	protected void finalize(){
        save();
	}

	public static void main(String args[]) {
	}
}
