package org.jfkd.banner.util;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Random;

public class Creativity{
	private static final String vars[]={
		"$campana","$grupo","$pagina","$banner",
		"$href","$iduser","$redirurl","$redirect"
	};

	private String campaign,url;
	private String image;
	private int height=0,width=0,fileLength=0;
	private String bannerDir,bannerUrl,redirUrl;
	private String file;
	private String alt;
	private String hrefUrl,htmlImage,hrefUrlTmp,htmlImageTmp;  
    private URL urlImage=null;
    private String contentType=null;
	private String imRedir,hrefRedir;
	private HashMap values=new HashMap();
	private StringBuffer out=new StringBuffer(),parsed=new StringBuffer();
	private StringBuffer href=null;
    private Random rand=new Random();
	

	public Creativity (String campaign, String bannerDir, String bannerUrl, String image, 
			String redirUrl, String url, String alt, String file){
		this.campaign=campaign;
		this.image=image;
		this.url=url;
		this.redirUrl=redirUrl+"?action=redirect";
		this.bannerUrl=bannerUrl;
		this.bannerDir=bannerDir;
		this.alt=alt;
		this.file=file;
		this.fileLength=file.length();
		
		hrefUrl=URLEncoder.encode(this.url);

		values.put("$banner",this.image);
		values.put("$redirurl",this.redirUrl);
		values.put("$href",hrefUrl);
		
		href=new StringBuffer(redirUrl);

		if (fileLength==0){
			getImageSize();
			htmlImage="<img src=\"";
            htmlImage+=(image.startsWith("http://"))
                ?image
                :bannerUrl+campaign+"/"+image;
            htmlImage+="\"";
			if (width>0 && height>0){
				htmlImage+=" width=\""+width+"\" height=\""+height+"\"";
			}
			htmlImage+=" alt=\""+alt+"\" border=0/>";
		}
	}

    public String getRedirectCode(String group,String page,String id){
        StringBuffer redir=new StringBuffer();
        redir.append(String.valueOf(System.currentTimeMillis()/1000));redir.append("|");
        redir.append(hrefUrl);redir.append("|");
        redir.append(campaign);redir.append("|");
        redir.append(group);redir.append("|");
        redir.append(page);redir.append("|");
        redir.append(URLEncoder.encode(image));redir.append("|");
        redir.append(id);
        return redir.toString();
    }
        
	public synchronized String outHTML(String group,String page,String id){
		String result="";

		href.setLength(0);
		out.setLength(0);

        if (hrefUrl.indexOf("%24random")<0){
            hrefUrlTmp=hrefUrl;
            htmlImageTmp=htmlImage;
        }
        else{
            String random=String.valueOf(rand.nextInt() & Integer.MAX_VALUE);
            hrefUrlTmp=Parse.variable(hrefUrl,"%24random",random,parsed);
            htmlImageTmp=Parse.variable(htmlImage,"$random",random,parsed);
        }

		href.append(redirUrl);
		href.append("&amp;href=");href.append(hrefUrlTmp);
		href.append("&amp;c=");href.append(campaign);
		href.append("&amp;g=");href.append(group);
		href.append("&amp;p=");href.append(page);
		href.append("&amp;b=");href.append(URLEncoder.encode(image));
		href.append("&amp;id=");href.append(id);
		
		values.put("$campana",campaign);
		values.put("$grupo",group);
		values.put("$pagina",page);
		values.put("$iduser",id);
		values.put("$redirect",href.toString());
		
		if (file.length()==0){
			out.append("<a href=\"");
			out.append(href.toString());
			out.append("\" target=\"_blank\">");
			out.append(htmlImageTmp);
			out.append("</a>");
		}
		else{
			out.append(file);
			try{
				int pos;
				for (int i=0;i<vars.length;i++){
					while ((pos=out.toString().indexOf(vars[i]))!=-1){
//System.out.println("Trying "+vars[i]+" "+pos+" "+values.get(vars[i]));
						out=out.replace(pos,pos+vars[i].length(),(String) values.get(vars[i]));
					}	
				}
			}
			catch (Exception e){
				System.out.println(e+" "+campaign+":"+image+"-"+out);
			}
		}
		result=out.toString();
		return (result);
	}

	public String debugtoString(){
		return "Campaign: "+campaign+
			" Image: "+image+" Url: "+
			url+" Alt: "+alt;
	} 

	public String toString(){
        // La version para los logs
		return URLEncoder.encode(image);
	}

	private void getImageSize(){
        String imageUrl=null;
        URLConnection connection=null;
		ImageInfo ii = new ImageInfo();
        if (image.startsWith("http://"))
        {
            imageUrl=image;
// Si no se garantiza una conexion externa, se bloquearia al intentar calcular el
// tamanyo
            return;
        }
        else{
    		imageUrl=(bannerDir.equals(""))?bannerUrl:("file:"+bannerDir);
    		imageUrl+="/"+campaign+"/"+image;
        }
        
		try{
			urlImage=new URL(imageUrl);
            connection=urlImage.openConnection();
			ii.setInput(connection.getInputStream());
		}
		catch (Exception e){
			System.err.println(e);
			return;
		}

		if (!ii.check()) {
			System.err.println(imageUrl+"...not a supported image file format.");
		}
		else{
//			System.out.println(imageUrl);
			width=ii.getWidth();
			height=ii.getHeight();
            contentType=ii.getMimeType();
		}
    }
    
    public URL getURL(){
        return urlImage;
    }
    
    public String getContentType(){
        return contentType;
    }
    	
	public static void main(String args[]) {
	}
}
