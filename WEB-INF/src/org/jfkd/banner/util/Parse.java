package org.jfkd.banner.util;


/**
 *
 * @author Armando Ramos <armando@hola.com>
 */


public class Parse {
    static final int MAX_RANDOM=Integer.MAX_VALUE;

    /**
     * Constructor for Parse.
     */
    public Parse() {
        super();
    }

    public static String variable(String st,String var,String value,StringBuffer parsed){
        int pos=st.indexOf(var);
        if (pos<0){
            return st;
        }
        parsed.setLength(0);
        parsed.append(st);

        return parsed.replace(pos,pos+var.length(),value).toString();
    }
        
        
    public static void main(String[] args) {
        System.out.println(Parse.variable("$random value","$random",null,new StringBuffer()));
    }
}
