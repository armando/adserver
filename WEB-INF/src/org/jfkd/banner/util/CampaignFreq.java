/*
 * Created on 18-nov-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.jfkd.banner.util;

/**
 * @author armando
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CampaignFreq {
    public abstract String getName();

    public abstract void addFreq(String freqId);

    public abstract boolean isValid(String freqId);

    public abstract void load();

    public abstract void save();
}