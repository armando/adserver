
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import org.jfkd.banner.util.FrequencyKeeper;
import org.jfkd.banner.util.FrequencyStorageCommand;
import org.prevayler.implementation.SnapshotPrevayler;

import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;




/**
 * 
 * 
**/
public class RebuildJE{
    private static Environment freqEnv= null;
    private static Database freqDb= null;
    
	public static void addLine(String log, int max){
        try{
            StringTokenizer st= new StringTokenizer(log,"/");
            String premes= st.nextToken();
            String mes= st.nextToken();
            String get= st.nextToken();
            String uid= st.nextToken();
            int count= 0;
            
            DatabaseEntry keyEntry = new DatabaseEntry();
            DatabaseEntry dataEntry = new DatabaseEntry();

            StringBinding.stringToEntry(uid, keyEntry);
            OperationStatus status= freqDb.get(null, keyEntry, dataEntry, LockMode.DEFAULT);
            
            if (status == OperationStatus.SUCCESS) {
                count= IntegerBinding.entryToInt(dataEntry);
            }

            if (count < max){
                IntegerBinding.intToEntry(++count, dataEntry);
//            System.out.println("Actualizando contador a "+count);
            
                status= freqDb.put(null, keyEntry, dataEntry);

            }
        }
		catch (Exception e){
		  System.out.println(e);
		}
	}



	public static void main(String[] args) {	
        String line=null;
		try {
            int max= Integer.parseInt(args[2]);
	        EnvironmentConfig envConfig = new EnvironmentConfig();
	        envConfig.setTransactional(true); 
	        envConfig.setAllowCreate(true);    
            File envDir= new File(args[1]);
            System.out.println(envDir.toString());

	        freqEnv = new Environment(envDir, envConfig);
	        
	        /* Make a database within that environment */
	        Transaction txn = freqEnv.beginTransaction(null, null);
	        DatabaseConfig dbConfig = new DatabaseConfig();
	        dbConfig.setTransactional(true); 
	        dbConfig.setAllowCreate(true);
	        freqDb = freqEnv.openDatabase(txn, "cookies.je", dbConfig);
	        
	        txn.commit();            
            
	        System.out.println("Leyendo "+args[0]);
            BufferedReader br= new BufferedReader(new FileReader(new File(args[0])));                
            while ((line= br.readLine())!=null){
                addLine(line, max);
            }
            br.close();
            
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally{
	           try {
	                freqDb.close();
	                freqEnv.close();
	            }
	            catch (DatabaseException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
		}
	}
}
