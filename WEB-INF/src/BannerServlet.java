import org.jfkd.banner.util.*;

import com.maxmind.geoip.LookupService;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 * 
 * 20020201 Agilbanner's true offspring is born
*/
public class BannerServlet extends HttpServlet {

	private static final String PAGE_PARAMETER = "pagina";
	private static final String UNRESOLVED = "unresolved";

// No se inicializan aqui porque deben renacer en initPages()
	private Map pages;
	private Map groups;
	private Map campaigns= new HashMap(); // menos esta, que es pesada
    private Map cstatus;
	private Map creativities;

	private Map camp_number;
	private Map camp_weight;
    
	private static HashMap domains,authorised=new HashMap();
    private static LookupService ls= null;
	
	private String real_remote_addr,remote_addr,user_agent,id;

	private final static String[] authorisedString={"127.0.0.1"};

	private CampaignStatus base=new CampaignStatus(new Campaign("base"),"base");

	/**
	 * @paramrequestInput request object containing any input parameters.
	 * @paramresponse  Output response object to receive the output.
	 *
	 * @exceptionIOException if any error writing the output.
	 * @exceptionServletException in case the servlet has a problem
	 *
	 */

	public void init() 
		throws ServletException{
        System.out.println("Initializing BannerServlet...");

		try{
			initPages();
		}
		catch (IOException e){
			System.err.println("init: "+e);
			return;
		}

//		domains=LoadIP.map();
        try {
            ls= new LookupService("/www/data/hola/geoip/GeoIP.dat", LookupService.GEOIP_MEMORY_CACHE);
            if (ls != null){
                System.out.println("Cargada geoip");
            }
        }
        catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
//domains=new HashMap();
		for (int i=0;i<authorisedString.length;i++){
			authorised.put(authorisedString[i],authorisedString[i]);
		}
	}


	/**
	 * @paramrequest   Input request object containing any input parameters.
	 * @paramresponse  Output response object to receive the output.
	 *
	 * @exceptionIOException if any error writing the output.
	 * @exceptionServletException in case the servlet has a problem
	 *
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
	// Set the content type to HTML.
		response.setContentType("text/html");

	// Get data for uninitialized groups and campaigns
		Map parameters=request.getParameterMap();
//		String page = request.getParameter(PAGE_PARAMETER);
//		String action = (request.getParameter("action")!=null)?request.getParameter("action"):"";
		String param[],tmp[];
		String action=null,code=null,salida=null;

		
		user_agent=request.getHeader("User-Agent");
		if (parameters.containsKey("http_user_agent")){
			tmp=(String[]) parameters.get("http_user_agent");
			user_agent=(tmp!=null)?URLDecoder.decode(tmp[0]):"User";
		}

        if (parameters.containsKey("code")){
            tmp=(String[]) parameters.get("code");
            code=(tmp!=null)?tmp[0]:null;
//            System.out.println(code);
        }

        if (parameters.containsKey("salida")){
            tmp=(String[]) parameters.get("salida");
            salida=(tmp!=null)?tmp[0]:null;
        }

		real_remote_addr=request.getRemoteAddr();
		if (parameters.containsKey("remote_addr")){
			tmp=(String[]) parameters.get("remote_addr");
			remote_addr=(tmp!=null)?tmp[0]:"127.0.0.1";
		}
		else{
			remote_addr=real_remote_addr;
		}

		id="anon";
		if (parameters.containsKey("id")){
			tmp=(String[]) parameters.get("id");
			id=(tmp!=null)?tmp[0]:"";
		}
		id=id.equals("")?"anon":id;

		if (parameters.containsKey("action")){
			tmp=(String[]) parameters.get("action");
			action=(tmp!=null)?tmp[0]:"";
			if (action.equals("save")){
				saveCampaigns();
			}

			if (action.equals("load") || action.equals("reload")){
	// && authorised.containsKey(real_remote_addr)){

                System.gc();
				saveCampaigns();
				if (action.equals("reload")){
				    campaigns= new HashMap();
				}
				initPages();
			}

			if (action.equals("redirect")){
                if (code==null){
    				String href="",c="",g="",p="",b="",gc="";
    				if (parameters.containsKey("gc")){
    					tmp=(String[]) parameters.get("gc");
    					gc=tmp[0];
    					if (gc.indexOf("/")>1){
    						c=gc.substring(0,gc.indexOf("/"));
    						g=gc.substring(gc.indexOf("/")+1);
    					}
    				}
    				if (parameters.containsKey("href")){
    					tmp=(String[]) parameters.get("href");
//    					href=URLDecoder.decode(tmp[0]);
    					href=tmp[0];
    				}
    				if (parameters.containsKey("c")){
    					tmp=(String[]) parameters.get("c");
    					c=tmp[0];
    				}
    				if (parameters.containsKey("g")){
    					tmp=(String[]) parameters.get("g");
    					g=tmp[0];
    				}
                    if (parameters.containsKey("p")){
                        tmp=(String[]) parameters.get("p");
                        p=tmp[0];
                    }
                    if (parameters.containsKey("b")){
                        tmp=(String[]) parameters.get("b");
                        b=tmp[0];
                    }
                    redirect(response,href,c,g,p,b,id);
                }
                else{
//                    System.out.println("Llamando "+code+":"+redirectCodes.get(code));
//                    redirect(response,(String) redirectCodes.get(code));
                }
				return;
			}
		}

		String page="";
		if (parameters.containsKey(PAGE_PARAMETER)){
			tmp=(String[]) parameters.get(PAGE_PARAMETER);
			page=tmp[0];
		}
		else{
			PrintWriter out = response.getWriter();
			out.println("No page");
//			out.close();
			return;
		}

        page= (page.charAt(0) != '/')?page:page.replace('/','=').substring(1);

		// Our old friend MSIE Crawler
		if (user_agent!=null && user_agent.indexOf("MSIECrawler")!=-1){
			sendBanner(response, base, page, id, null, salida, false);
			return;
		}

		// System.out.println("P�gina recibida: "+page+"--");

		if (!pages.containsKey(page)){
			try{
				initCampaigns(page);
				// System.out.println("No existia "+page);
			}catch (Exception e){
				System.err.println("Page: "+e);
				return;
			}
		}

		ArrayList listgroups= (ArrayList) pages.get(page);

// System.out.println("Grupos: "+listgroups);
		
/***
	A bit of magic to choose the right campaign
	TreeSet automagically orders the campaigns active (and valid) 
    in this page's groups according to their
	differences from the aimed average (CampaignStatus class)
***/	
		TreeSet considered=new TreeSet();
		considered.add(base);
		CampaignStatus cs;
		int groupsSize=listgroups.size();
		ArrayList listCampaigns;
		int campaignsSize;

// Impressions campaigns

		for (int i=0;i<groupsSize;i++){
			listCampaigns=(ArrayList) camp_number.get(listgroups.get(i));
			campaignsSize=listCampaigns.size();
			for (int j=0;j<campaignsSize; j++){
				cs=(CampaignStatus) listCampaigns.get(j);
//                System.out.println("Compruebo "+cs.getCampaign());
				if (cs.isValid(remote_addr, user_agent, id, domains, ls)){
					considered.add(cs);
//					System.out.println("Anyado "+cs.getCampaign()+":"+cs.getDiff());
					
				}
			}
		}


		CampaignStatus topcampaign= (CampaignStatus) considered.first();


//System.out.println("Diff:"+topcampaign.getCampaign()+"--"+topcampaign.getDiff());


// Weights campaigns
		if (topcampaign.getDiff()<=0.0){
			ArrayList considered_weight=new ArrayList();
			int total=0;
			for (int i=0;i<groupsSize; i++){
				listCampaigns=(ArrayList) camp_weight.get(listgroups.get(i));
				campaignsSize=listCampaigns.size();
				for (int j=0;j<campaignsSize; j++){
					cs= (CampaignStatus) listCampaigns.get(j);
					//System.out.println("Considerando: "+cs.getCampaign());
					if (cs.isValid(remote_addr,user_agent,id,domains, ls)){
					    //System.out.println("Aceptando: "+cs.getCampaign());
						considered_weight.add(cs);
						total+=cs.getWeight();
					}
				}
			}

			//System.out.println("Por pesos: "+considered_weight+" Total: "+total);

			if (total>0){
				Random rand=new Random();
				//Integer.MAX_VALUE is to mask off sign bit
				int n=1+(rand.nextInt() & Integer.MAX_VALUE)%total;
			
				int weight=0;
				for (int i=0;i<=considered_weight.size(); i++){
					cs=(CampaignStatus) considered_weight.get(i);
					weight+=cs.getWeight();
					if (n<=weight){
						topcampaign=cs;
						break;
					}
				}
			}
		}

		sendBanner(response, topcampaign, page, id, code, salida, true);
		
		return;
	}

	private void sendBanner(HttpServletResponse response, 
		CampaignStatus campaign, String page, String id, String code, String salida, boolean log)
		throws IOException{

		String chosen=campaign.getCampaign();
//		System.out.println("Elegida="+chosen);
		Creativity banner=((Creativities) creativities.get(chosen)).chooseCreativity();
            PrintWriter out = response.getWriter();
            String html=banner.outHTML(campaign.getGroup(),page,id);
            response.setContentType("text/html");
            response.setContentLength(html.length());
            response.flushBuffer();
        	if (salida!=null && salida.equals("js")){
        		out.print("document.write(\"");
    	        out.print(replace(html,"\"","\\\""));
        		out.print("\")");
        	}
        	else{
        		out.print(html);
        	}
            out.flush();
            out.close();
            out=null;
            html=null;

        try{

		// Loggeable campaign
    		if (log){
    			campaign.addImp(remote_addr,user_agent,page,banner.toString(),id);
    		}
        }
        catch (Exception e){}
    		chosen=null;
    	}		

    static String replace(String str, String pattern, String replace) {
        int s = 0;
        int e = 0;
        StringBuffer result = new StringBuffer();
    
        while ((e = str.indexOf(pattern, s)) >= 0) {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e+pattern.length();
        }
        result.append(str.substring(s));
        return result.toString();
    }
   
	public synchronized void initPages()
		throws IOException{

		pages=new HashMap();
		groups=new HashMap();
//		campaigns=new HashMap();
        cstatus=new HashMap();
		creativities=new HashMap();

		creativities.put("base",new Creativities("base"));

		camp_number=new HashMap();
		camp_weight=new HashMap();

		ArrayList listpages=LoadPages.pages();
		int numpages=listpages.size();
		String page=null;

		for (int i=0; i<numpages; i++){
			try{
				page=(String) listpages.get(i);
				initCampaigns(page);
			} catch (Exception e){
				System.err.println("INIT: initPages "+e+i+page);
			}
		}
	}

	private synchronized void initCampaigns(String page) 
		throws IOException
	{
		List lgroups=LoadGroups.groups(page);
//System.out.println("Init-campaigns..."+lgroups);
		pages.put(page,lgroups);
		ArrayList listcs;
		for (int i=0;i<lgroups.size();i++){
			String group= (String) lgroups.get(i);
			if (!groups.containsKey(group)){
				List lcamps=LoadCampaigns.campaigns(group);
				groups.put(group,new Integer(1));
				camp_number.put(group,new ArrayList());
				camp_weight.put(group,new ArrayList());
				for (int j=0;j<lcamps.size();j++){
					String campaign= (String) lcamps.get(j);
                    if (!campaigns.containsKey(campaign)){
                        campaigns.put(campaign,new Campaign(campaign));
                    }
                    CampaignFreq c=(CampaignFreq) campaigns.get(campaign);
					String code=campaign+"/"+group;
					if (!cstatus.containsKey(code)){
						CampaignStatus cs=new CampaignStatus(c,group);
						cstatus.put(code,cs);
						if (!creativities.containsKey(campaign)){
							creativities.put(campaign,new Creativities(campaign));
						}
						if (cs.isWeight()){
							listcs= (ArrayList) camp_weight.get(group);
							listcs.add(cs);
//System.out.println("init cstatus (weight):"+group+":"+cs.getCampaign());
							camp_weight.put(group,listcs);
						}
						else{
							listcs= (ArrayList) camp_number.get(group);
							listcs.add(cs);
//System.out.println("init cstatus (imps):"+group+":"+cs.getCampaign());
							camp_number.put(group,listcs);
						}
					}
				}
			}
		}
	}

	private synchronized void saveCampaigns(){
        System.out.println("Grabando campanas ");
		for (Iterator i=cstatus.keySet().iterator();i.hasNext();){
			CampaignStatus cs= (CampaignStatus) cstatus.get(i.next());
			cs.save();
		}
	}

	private void redirect(HttpServletResponse response,String href,
			String campaign, String group, String page, String banner, String id){

	    if (!pages.containsKey(page)){
				try{
					initCampaigns(page);
				}catch (Exception e){
					System.err.println("Page: "+e);
				}
		}

		try{
//		    System.out.println("Status de "+campaign+"/"+group);
			CampaignStatus cs= (CampaignStatus) cstatus.get(campaign+"/"+group);
			if (href.startsWith("http://62.22.171.13/banners/")){
				cs.addImp(remote_addr,"",page,banner,id);
			}
			else{
				cs.addClick(remote_addr,page,banner,id);
			}
//			System.out.println("About to redirect to "+href);
			response.sendRedirect(href);
			return;
		}catch (Exception e){
			System.err.println(e+"- Redirect: "+campaign+"/"+group+"/"+
				page+"/"+banner+"-"+href+"-"+id);
		}

		try{
//		    System.out.println("Redirect de emergencia ");
			response.sendRedirect("http://www.hola.com");
		}catch (Exception e){
			System.out.println(e+":");
			System.err.println("...Error en redirect de emergencia!");
		}
	}

    private void redirect(HttpServletResponse response,String href){
        StringTokenizer st=new StringTokenizer(href,"|");
        String date=st.nextToken();
//        System.out.println("Fecha de codigo "+date);
        redirect(response,URLDecoder.decode(st.nextToken()),st.nextToken(),st.nextToken(),
            st.nextToken(),st.nextToken(),st.nextToken());
    }

	
	public void destroy(){
		saveCampaigns();
	}
}
